package jp.alhinc.ueda_genta.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {

	public static void main(String[] args) {
		//System.out.println("ここにあるファイルを開きます => " + args[0]);
		HashMap<String, String> map = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();



		BufferedReader br = null;
		try {
			File file = new File(args[0],"branch.lst");
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}


			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				//System.out.println(line);
				String[] arr = line.split(",",0);
				if (!arr[0].matches("^[0-9]{3}$") ) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				for (int i = 0; i < arr.length; i++) {
					if (arr.length >= 3) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
				}
				map.put(arr[0],arr[1]);
				salesMap.put(arr[0],0L);
			}

		}catch(IOException e) {

		}finally{
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
				}
			}
		}

		File file_2 = new File(args[0]);
		File salesfiles[] = file_2.listFiles();


		for (int i = 0; i < salesfiles.length; i++){
			if (salesfiles[i].getName().matches("^[0-9]{8}.rcd$")){
				//System.out.println(salesfiles[i].getName());

				try {
					FileReader fr = new FileReader(salesfiles[i]);
					br = new BufferedReader(fr);
					List<String> list = new ArrayList<String>();
					String line;
					while((line = br.readLine()) != null) {
						list.add(line);
					}

					if (!map.containsKey(list.get(0))) {
						System.out.println(salesfiles[i] + "の支店コードが不正です");
						return;
					}

					long added = salesMap.get(list.get(0));
					added += Long.parseLong(list.get(1));
					salesMap.put(list.get(0),added);

					String sum = Long.toString(added);
					if (10 < sum.length()) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}

					if(list.size() >=3) {
						System.out.println(salesfiles[i] + "のフォーマットが不正です");
						return;
					}

				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;

				}finally{
					if(br != null) {
						try {
							br.close();
						} catch(IOException e) {
						}
					}
				}
			}
		}

		try{
			File file = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			for (String key : salesMap.keySet()) {
				bw.write(key + ",");
				bw.write(map.get(key) + ",");
				String line = Long.toString(salesMap.get(key));
				bw.write(line);
				bw.newLine();
			}
			bw.close();

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}

	}

}



